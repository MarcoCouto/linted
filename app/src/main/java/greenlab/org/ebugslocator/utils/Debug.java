package greenlab.org.ebugslocator.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marco on 13-03-2018.
 */

public class Debug {

    public static List<String> mDebugFiles = new ArrayList<>();
    static {
        mDebugFiles.add("TVShowSummary.java");
    }

    public static List<String> getDebugFiles() {
        return mDebugFiles;
    }

    public static boolean containsFile(String filename) {
        for (String f : mDebugFiles) {
            if (filename.endsWith(f)) return true;
        }
        //return mDebugFiles.contains(filename);
        return false;
    }

    public static void printDebug(String tag, String content, int level) {
        String tabs = "";
        for (int i = 0; i < level; i++) {
            tabs+="\t";
        }

        System.out.println("<< "+tag+" >>");

        System.out.println(tabs+content);

        System.out.println("<</ "+tag+" >>\n");
    }
}
