package greenlab.org.ebugslocator.utils;

import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Location;
import com.android.tools.lint.detector.api.TextFormat;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiVariable;

/**
 * Created by marco on 16-03-2018.
 */

public class Reporter {

    public static void reportIssue(JavaContext context, Issue issue, PsiExpression expression) {
        Location loc = context.getLocation(expression);
        context.report(issue,
                Location.create(context.file,
                        loc.getStart(),
                        loc.getEnd()),
                issue.getBriefDescription(TextFormat.TEXT));
    }

    public static void reportIssue(JavaContext context, Issue issue, PsiVariable variable) {
        Location loc = context.getLocation(variable);
        context.report(issue,
                Location.create(context.file,
                        loc.getStart(),
                        loc.getEnd()),
                issue.getBriefDescription(TextFormat.TEXT));
    }

    public static void reportIssue(JavaContext context, Issue issue, PsiMethod method) {
        Location loc = context.getLocation(method);
        context.report(issue,
                Location.create(context.file,
                        loc.getStart(),
                        loc.getEnd()),
                issue.getBriefDescription(TextFormat.TEXT));
    }

}
