package greenlab.org.ebugslocator.detectors;

import com.android.annotations.NonNull;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Location;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.android.tools.lint.detector.api.TextFormat;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiCodeBlock;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiReturnStatement;
import com.intellij.psi.PsiStatement;

import java.io.File;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import greenlab.org.ebugslocator.utils.Reporter;


/**
 * Created by marco on 28-02-2018.
 */

public class InternalGetterDetector extends Detector implements Detector.JavaPsiScanner {
    private static final Class<? extends Detector> DETECTOR_CLASS = InternalGetterDetector.class;
    private static final EnumSet<Scope> DETECTOR_SCOPE = Scope.JAVA_FILE_SCOPE;

    private static final Implementation IMPLEMENTATION = new Implementation(
            DETECTOR_CLASS,
            DETECTOR_SCOPE
    );

    private static final String ISSUE_ID = "InternalGetter";
    private static final String ISSUE_DESCRIPTION = "Avoid Using Internal Getters";
    private static final String ISSUE_EXPLANATION = "Calling a getter inside the same class leads to unnecessary computational effort. Therefore, the execution time and energy consumption of your application may also increase";
    private static final Category ISSUE_CATEGORY = Category.PERFORMANCE;
    private static final int ISSUE_PRIORITY = 5;
    private static final Severity ISSUE_SEVERITY = Severity.WARNING;

    public static final Issue ISSUE = Issue.create(
            ISSUE_ID,
            ISSUE_DESCRIPTION,
            ISSUE_EXPLANATION,
            ISSUE_CATEGORY,
            ISSUE_PRIORITY,
            ISSUE_SEVERITY,
            IMPLEMENTATION
    );

    private static HashMap<File, Set<String>> getters = new HashMap<>();

    public InternalGetterDetector() {
    }

    private void debugGetters() {
        for (Map.Entry<File, Set<String>> entry : getters.entrySet()){
            System.out.println(entry.getKey().getName());
            for (String s : entry.getValue()) {
                System.out.println("\t"+s);
            }
        }
    }

    /**
     * Selects the AST nodes for the detector to reach.
     * @return
     */
    @Override
    public List<Class<? extends PsiElement>> getApplicablePsiTypes() {
        return Arrays.<Class<? extends PsiElement>>asList(PsiMethod.class, PsiMethodCallExpression.class);
    }

    @Override
    public void afterCheckProject(Context context) {
        if (context.getDriver().getPhase() == 1) {
            //debugGetters();
            context.getDriver().requestRepeat(this, DETECTOR_SCOPE);
        }else{
            //getters.clear();
        }
        super.afterCheckProject(context);
    }

    /**
     * Creates a Parse Tree visitor to analyze the AST of a file.
     * @param context
     * @return
     */
    @Override
    public JavaElementVisitor createPsiVisitor(@NonNull JavaContext context) {
        int phase = context.getDriver().getPhase();
        if (phase == 1){
            // First AST traversal.
            // We are now looking for "Getters" to put in the getters variable.
            return new MethodChecker(context, phase);

        }else{
            // Second AST traversal.
            // We will check for calls of getters inside the corresponding class to report.
            return new CallChecker(context, phase);
        }
    }

    private static class MethodChecker extends JavaElementVisitor{

        private final String mReturnClass = "com.intellij.psi.impl.source.tree.java.PsiReturnStatementImpl";
        private final String mReferenceClass = "com.intellij.psi.impl.source.tree.java.PsiReferenceExpressionImpl";
        private final JavaContext mContext;
        private int phase;

        public MethodChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
        }

        @Override
        public void visitMethod(PsiMethod method) {
            if (this.phase == 1) {
                if (this.isGetter(method)) {
                    File file = mContext.getLocation(method).getFile();
                    String methodName = method.getName();
                    if (getters.containsKey(file)){
                        getters.get(file).add(methodName);
                    }else{
                        Set<String> methodsSet = new HashSet<>();
                        methodsSet.add(methodName);
                        getters.put(file, methodsSet);
                    }
                }
            }
            super.visitMethod(method);
        }

        private boolean isGetter(PsiMethod method) {
            //System.out.println("\t\t> "+ method.getName() +" <");
            PsiCodeBlock body = method.getBody();
            if (body == null){
                return false;
            }
            PsiStatement statements[] = body.getStatements();
            if (statements != null) {
                if (statements.length != 1){
                    return false;
                } else if (statements[0].getClass().getTypeName().equals(this.mReturnClass)) {
                    PsiReturnStatement statement = (PsiReturnStatement)statements[0];
                    if (statement.getReturnValue().getClass().getTypeName().equals(this.mReferenceClass)){
                        return true;
                    }
                }
            }
            return false;
        }

    }

    private static class CallChecker extends JavaElementVisitor{

        private final JavaContext mContext;
        private int phase;

        public CallChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
        }

        @Override
        public void visitMethodCallExpression(PsiMethodCallExpression expression) {
            if (phase != 1) {
                File file = mContext.getLocation(expression).getFile();
                if (getters.containsKey(file)) {
                    Set<String> getterMethods = getters.get(file);
                    String qualifiedName = expression.getMethodExpression()
                                                     .getQualifiedName()
                                                     .replace("this.", "");
                    if (getterMethods.contains(qualifiedName)) {
                        Reporter.reportIssue(mContext, ISSUE, expression);
                    }
                }
            }
            super.visitMethodCallExpression(expression);
        }
    }
}
