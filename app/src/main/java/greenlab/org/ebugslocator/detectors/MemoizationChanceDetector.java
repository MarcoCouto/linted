package greenlab.org.ebugslocator.detectors;

import com.android.annotations.NonNull;
import com.android.tools.lint.detector.api.Category;
import com.android.tools.lint.detector.api.Context;
import com.android.tools.lint.detector.api.Detector;
import com.android.tools.lint.detector.api.Implementation;
import com.android.tools.lint.detector.api.Issue;
import com.android.tools.lint.detector.api.JavaContext;
import com.android.tools.lint.detector.api.Scope;
import com.android.tools.lint.detector.api.Severity;
import com.intellij.psi.JavaElementVisitor;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiExpression;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiLocalVariable;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.PsiParameter;
import com.intellij.psi.PsiParameterList;
import com.intellij.psi.PsiReferenceExpression;
import com.intellij.psi.PsiType;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import greenlab.org.ebugslocator.utils.Reporter;
import greenlab.org.ebugslocator.utils.StringUtils;

/**
 * Created by marco on 02-03-2018.
 */

public class MemoizationChanceDetector extends Detector implements Detector.JavaPsiScanner {
    private static final Class<? extends Detector> DETECTOR_CLASS = MemoizationChanceDetector.class;
    private static final EnumSet<Scope> DETECTOR_SCOPE = Scope.JAVA_FILE_SCOPE;

    private static final Implementation IMPLEMENTATION = new Implementation(
            DETECTOR_CLASS,
            DETECTOR_SCOPE
    );

    private static final String ISSUE_ID = "MemoizationChance";
    private static final String ISSUE_DESCRIPTION = "The method is memoizable";
    private static final String ISSUE_EXPLANATION =
            "Methods that work as simples functions are good targets to " +
            "exploit memoization for performance enhancement." // TODO: improve explanation
            ;
    private static final Category ISSUE_CATEGORY = Category.PERFORMANCE;
    private static final int ISSUE_PRIORITY = 5;
    private static final Severity ISSUE_SEVERITY = Severity.WARNING;

    public static final Issue ISSUE = Issue.create(
            ISSUE_ID,
            ISSUE_DESCRIPTION,
            ISSUE_EXPLANATION,
            ISSUE_CATEGORY,
            ISSUE_PRIORITY,
            ISSUE_SEVERITY,
            IMPLEMENTATION
    );

    private HashMap<String, Boolean> classVars;
    private HashMap<String, Boolean> classMethods;
    private HashMap<String, Set<String>> methodCalls;
    private HashMap<String, HashSet<String>> methodLocalVars;

    public static HashSet<String> nativeTypes = new HashSet<>();

    public MemoizationChanceDetector() {
        this.classVars = new HashMap<>();
        this.classMethods = new HashMap<>();
        this.methodCalls = new HashMap<>();
        this.methodLocalVars = new HashMap<>();

        nativeTypes.add("boolean");
        nativeTypes.add("int");
        nativeTypes.add("short");
        nativeTypes.add("long");
        nativeTypes.add("float");
        nativeTypes.add("double");
        nativeTypes.add("byte");
        nativeTypes.add("char");
        // Java Native Wrapper Classes
        nativeTypes.add("java.lang.Boolean");
        nativeTypes.add("java.lang.Integer");
        nativeTypes.add("java.lang.Short");
        nativeTypes.add("java.lang.Long");
        nativeTypes.add("java.lang.Float");
        nativeTypes.add("java.lang.Double");
        nativeTypes.add("java.lang.Byte");
        nativeTypes.add("java.lang.Character");
        nativeTypes.add("java.lang.String");
    }

    /**
     * Selects the AST nodes for the detector to reach.
     * @return
     */
    /* FIXME: Variables with names already used as filed variables.
            When there's a class field named "X", and in a method a new variable
            is declared with the same name "X", they both are considered as fields */
    @Override
    public List<Class<? extends PsiElement>> getApplicablePsiTypes() {
        return Arrays.<Class<? extends PsiElement>>asList(PsiMethod.class,
                                PsiMethodCallExpression.class,
                                PsiReferenceExpression.class,
                                PsiField.class,
                                PsiLocalVariable.class,
                                PsiParameter.class);
    }

    @Override
    public void afterCheckProject(Context context) {
        if (context.getDriver().getPhase() < 3) {
            context.getDriver().requestRepeat(this, DETECTOR_SCOPE);
        }else {
            //debug();
        }
        super.afterCheckProject(context);
    }

    /**
     * Creates a Parse Tree visitor to analyze the AST of a file.
     * @param context
     * @return
     */
    @Override
    public JavaElementVisitor createPsiVisitor(@NonNull JavaContext context) {
        int phase = context.getDriver().getPhase();
        if (phase == 1){
            // First AST traversal.
            // Look for class variables (really quick).
            return new ClassVarsChecker(context, phase);
        }else if (phase == 2){
            // Second AST traversal.
            // Look for variables that are updated inside a loop, and
            // also gather info about ALL existing method calls.
            return new MemoizationChecker(context, phase);
        }else {
            // Third (final) AST traversal.
            // Check if the previously detected method calls can be
            // passed outside the loop
            checkInnerCalls();
            return new ReportChecker(context, phase);
        }
    }

    /* Helper Methods */
    private void checkInnerCalls() {
        for (String m : methodCalls.keySet()) {
            Boolean check = classMethods.get(m);
            if (check != null && check) {
                // Only verify the method calls inside a method IF the memoization test
                // didn't fail for any other reason.
                boolean allCallsMemoizable = true;

                for (String call : methodCalls.get(m)) {
                    if (!allCallsMemoizable) break;

                    if (classMethods.containsKey(call)) {
                        // if a called method is memoizable, the callee method also is.
                        allCallsMemoizable = classMethods.get(call);
                    } else if (methodLocalVars.get(m) != null && methodLocalVars.get(m).contains(call)) {
                        allCallsMemoizable = true;
                    } else {
                        allCallsMemoizable = false;
                    }
                }

                classMethods.put(m, allCallsMemoizable);
            }
        }
    }

    public PsiMethod getParentMethod(PsiElement elem) {
        if (elem == null) return null;

        if (elem instanceof PsiMethod) return ((PsiMethod) elem);

        PsiElement p = elem.getParent();
        return getParentMethod(p);
    }

    public String getParentMethodName(PsiElement elem, JavaContext context) {
        PsiMethod method = getParentMethod(elem);
        if (method == null) return "";

        String filename = StringUtils.getShortFileName(context.getJavaFile());
        String packageName = context.getJavaFile().getPackageName();
        return  packageName + "." + filename + "." + method.getName();
    }

    /** AST Visitors **/
    private class ClassVarsChecker extends JavaElementVisitor {

        private final JavaContext mContext;
        private int phase;
        private String filename;

        public ClassVarsChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
            this.filename = StringUtils.getShortFileName(this.mContext.getJavaFile());
        }


        private void addLocalVar(String methodName, String varName) {
            if (methodLocalVars.containsKey(methodName)) {
                methodLocalVars.get(methodName).add(varName);
            } else {
                HashSet<String> calls = new HashSet<>();
                calls.add(varName);
                methodLocalVars.put(methodName, calls);
            }
        }

        private Set<String> getMethodArgs(PsiMethod method) {
            Set<String> args = new HashSet<>();
            PsiParameter[] params = method.getParameterList().getParameters();
            for (PsiParameter p : params) {
                args.add(p.getName());
            }
            return args;
        }

        @Override
        public void visitField(PsiField field) {
            String className = mContext.getJavaFile().getPackageName() + "." + this.filename;
            classVars.put(className + "." + field.getName(), field.getModifierList().hasModifierProperty("final"));
            super.visitVariable(field);
        }

        @Override
        public void visitLocalVariable(PsiLocalVariable variable) {
            String parentMethod = getParentMethodName(variable, mContext);
            if (!parentMethod.equals("")) {
                addLocalVar(parentMethod, variable.getName());
            }
            super.visitLocalVariable(variable);
        }

        @Override
        public void visitParameter(PsiParameter parameter) {
            String visitingMethod = getParentMethodName(parameter, this.mContext);
            if (!visitingMethod.equals("")) {
                addLocalVar(visitingMethod, parameter.getName());
            }
            super.visitParameter(parameter);
        }

        @Override
        public void visitMethod(PsiMethod method) {
            String visitingMethod = getParentMethodName(method, this.mContext);

            if (!method.isConstructor() && !visitingMethod.equals("")) {
                classMethods.put(visitingMethod, true);
            }
            super.visitMethod(method);
        }
    }

    private class MemoizationChecker extends JavaElementVisitor {

        private final JavaContext mContext;
        private int phase;
        private String filename;

        public MemoizationChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
            this.filename = StringUtils.getShortFileName(this.mContext.getJavaFile());
        }

        /* Helper methods */
        private boolean areParametersNative(PsiParameterList parameterList) {
            PsiParameter[] parameters = parameterList.getParameters();

            if (parameters.length == 0) return false;

            for (int i = 0; i < parameterList.getParametersCount(); i++) {
                PsiParameter param = parameters[i];
                String typeName = param.getType().getCanonicalText();
                if (! nativeTypes.contains(typeName)) return false;
            }

            return true;
        }

        private boolean isReturnTypeNative(PsiType returnType) {
            return nativeTypes.contains(returnType.getCanonicalText());
        }

        private void addMethodCall(String calleeMethod, String call) {
            if (methodCalls.containsKey(calleeMethod)) {
                methodCalls.get(calleeMethod).add(call);
            } else {
                HashSet<String> calls = new HashSet<>();
                calls.add(call);
                methodCalls.put(calleeMethod, calls);
            }
        }

        @Override
        public void visitMethod(PsiMethod method) {
            String visitingMethod = getParentMethodName(method, mContext);

            if (!method.isConstructor() && !visitingMethod.equals("")) {
                boolean goFurther = isReturnTypeNative(method.getReturnType()) &&
                                    areParametersNative(method.getParameterList());
                classMethods.put(visitingMethod, goFurther);
            }

            super.visitMethod(method);
        }

        @Override
        public void visitReferenceExpression(PsiReferenceExpression expression) {
            String visitingMethod = getParentMethodName(expression, mContext);
            Boolean check = classMethods.get(visitingMethod);
            if (!visitingMethod.equals("") && check != null && check) {
                // Reaching this point means that the method has
                // native return type and native arguments.
                String qualifiedVarName = expression.getQualifiedName().replace("this.", "");
                String exprVarName = expression.getText().replace("this.", "").replaceAll("\\..+", "");
                Set<String> methodVars = methodLocalVars.get(visitingMethod);

                boolean isLocalVar = methodVars != null &&
                        (methodVars.contains(qualifiedVarName) || methodVars.contains(exprVarName));
                boolean isFinalVar = classVars.keySet().contains(qualifiedVarName) && (classVars.get(qualifiedVarName));
                boolean isMethodCall = classMethods.containsKey(qualifiedVarName);
                if (!isLocalVar && !isFinalVar && !isMethodCall) {
                    classMethods.put(visitingMethod, false);
                }
            }

            super.visitReferenceExpression(expression);
        }

        @Override
        public void visitMethodCallExpression(PsiMethodCallExpression expression) {
            String visitingMethod = getParentMethodName(expression, mContext);
            if (!visitingMethod.equals("")) {

                PsiExpression callVarExp = expression.getMethodExpression().getQualifierExpression();
                String callVariable = "";
                if (callVarExp != null)
                    callVariable = callVarExp.getText().replace("this.", "");

                String call = expression.getMethodExpression().getQualifiedName();
                if (!callVariable.equals("")) call = callVariable;

                addMethodCall(visitingMethod, call);
            }
            super.visitMethodCallExpression(expression);
        }
    }

    private class ReportChecker extends JavaElementVisitor {

        private final JavaContext mContext;
        private int phase;
        private String filename;

        public ReportChecker(JavaContext context, int phase) {
            this.mContext = context;
            this.phase = phase;
            this.filename = StringUtils.getShortFileName(this.mContext.getJavaFile());
        }

        @Override
        public void visitMethod(PsiMethod method) {
            String name = getParentMethodName(method, mContext);
            Boolean containsBody = method.getBody() != null &&
                                method.getBody().getStatements() != null &&
                                method.getBody().getStatements().length > 1;
            Boolean check = classMethods.containsKey(name) && classMethods.get(name);
            if (!name.equals("") && containsBody && check) {
                Reporter.reportIssue(mContext, ISSUE, method);
            }
            super.visitMethod(method);
        }

    }
}
